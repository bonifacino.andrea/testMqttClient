var mqtt = require('mqtt');
var messages = require('./exampleMessage_pb.js'); // reference: https://www.npmjs.com/package/google-protobuf
var client1  = mqtt.connect('mqtt://2.235.171.187');
var client2  = mqtt.connect('mqtt://2.235.171.187');
var str2ab = require('string-to-arraybuffer');
var ta2b = require('typedarray-to-buffer');

client1.on('connect', function () {
  client1.subscribe({'topicClient2': 1});
  var cont = 1;
  var intervalClient1 = setInterval(function(){
    var messageText = "message" + (cont++);
    var message = new messages.exampleMessage();
    message.setClientid("client1Id");
    message.setMessage(messageText);
    var bytes = message.serializeBinary();
    client1.publish('topicClient1', ta2b(bytes));
    if (messageText=="message10"){
        client1.end();
        clearInterval(this);
    }
  }, 5000);
})

client1.on('message', function (topic, message) {
  // message is Buffer
  var arrayBuffer = new Uint8Array(message);
  var deserialized = messages.exampleMessage.deserializeBinary(arrayBuffer);
  console.log("***** CLIENT1 ***** RECEVED THIS MESSAGE: ");
  console.log("ClientID: ", deserialized.getClientid());
  console.log("Message: ", deserialized.getMessage());
})

client2.on('connect', function () {
  client2.subscribe({'topicClient1': 1});
  var cont = 1;
  var intervalClient2 = setInterval(function(){
    var messageText = "message" + (cont++);
    var message = new messages.exampleMessage();
    message.setClientid("client2Id");
    message.setMessage(messageText);
    var bytes = message.serializeBinary();
    client2.publish('topicClient2', ta2b(bytes));
    if (messageText=="message10"){
        client2.end();
        clearInterval(this);
    }
  }, 10000);
})

client2.on('message', function (topic, message) {
  // message is Buffer
  var arrayBuffer = new Uint8Array(message);
  var deserialized = messages.exampleMessage.deserializeBinary(arrayBuffer);
  console.log("----- client2 ----- receved this message: ");
  console.log("ClientID: ", deserialized.getClientid());
  console.log("Message: ", deserialized.getMessage());
})
